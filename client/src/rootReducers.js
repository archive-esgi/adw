import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form'; // stocke les entrées dans les champs de nos form
import AuthentificationReducer from './app/authentification/duck/AuthReducer';
import ErrorReducer from './app/common/error/duck/ErrorReducer';
import MissionReducer from './app/mission/duck/MissionReducer';
const rootReducer = combineReducers({
    // attribuer chaque reducers à une "variable"
    authentification: AuthentificationReducer,
    error: ErrorReducer,
    mission: MissionReducer,
    form: form
});

export default rootReducer;