/* fonctions de validations pour l'auth */

export const validateEmail = value =>
    // si pas de valeur ou email invalide
    (!value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Adresse email invalide'
        : undefined);

export const validateNotEmpty = value => 
    (value)
        ? undefined
        : 'Champs requis'

// vérifie que 2 champs sont égaux
export const validateEqual = (value1, value2) => 
    (value1 && value2 && value1 === value2)
        ? undefined
        : 'Les valeurs ne sont pas indentiques'