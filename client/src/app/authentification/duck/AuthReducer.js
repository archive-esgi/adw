import {SET_AUTHENTIFICATION} from './AuthTypes';

const initialState = {
    isLoggedIn: false
}

/* reducer */
export default function AuthentificationReducer(state = initialState, action) {
    switch(action.type) {
        /* nouvel état */
        case SET_AUTHENTIFICATION: 
            return {
                isLoggedIn: action.payload
            }
        default:
            return state;
    }
}