import React, {Component} from 'react';
import {Button, TextField, FormControlLabel, Checkbox, Grid, Box, Typography, Container} from '@material-ui/core/';
import CssBaseline from '@material-ui/core/CssBaseline';
import {Link} from 'react-router-dom';
import {withStyles} from '@material-ui/core/styles';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import Copyright from '../common/copyright/Copyright';
import Error from '../common/error/Error';
import * as actions from './duck/AuthAction';
import signinBg from '../../assets/img/auth-bg.jpg';
import adwMainLogo from '../../assets/img/logo/adw-main-logo-70.svg';

const styles = theme => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    mainBg: {
        backgroundImage: `url(${signinBg})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        display: 'flex',
        justifyContent: 'center', 
        alignItems: 'center', 
        flexDirection: 'column',
        height: '100vh'
    },
    link: {
        textDecoration: 'none'
    }
});

const fields = {
    username: 'username',
    password: 'password'
};

class SigninPage extends Component {
    handleSubmit = credentials => {
        console.log(credentials);
        this.props.signinUser(credentials, this.props.history);
    }

    renderInputComponent = field => {
        return (
            <TextField
                {...field.input}
                label={field.label}
                type={field.type}
                variant="outlined"
                margin="normal"
                required
                fullWidth
            />
        );
    }

    render() {
        const {classes, handleSubmit,} = this.props;

        return (
            <React.Fragment>
                <div className={classes.mainBg}>
                    <Container component="main" maxWidth="xs">
                    <CssBaseline />
                    <div className={classes.paper}>
                            <Link to="/">
                                <Button href="/">
                                    <img src={adwMainLogo} width="230" height="130" />
                                </Button>
                            </Link>
                        <Typography component="h1" variant="h5">
                            Connexion
                        </Typography>
                        <Error />
                        <form className={classes.form} onSubmit={handleSubmit(this.handleSubmit)}>
                            <Field
                                name={fields.username}
                                type="text"
                                label="Adresse-email"
                                component={this.renderInputComponent}
                                
                            />
                            <Field
                                name={fields.password}
                                type="password"
                                label="Mot de passe"
                                component={this.renderInputComponent}
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            >
                                Connexion
                            </Button>
                            <Grid container justify="flex-end">
                                <Grid item>
                                    <Link to="/signup" variant="body2" className={classes.link}>
                                        Vous n'avez pas de compte ? Inscrivez-vous !
                                    </Link> 
                                </Grid>
                                <Grid item>
                                    <Link to="/forget-password" variant="body2" className={classes.link}>
                                        Mot de passe oubliée ?
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                    <Box mt={8}>
                        <Copyright />
                    </Box>
                </Container>
                </div>
            </React.Fragment>
        );
    }
}

const signinForm = reduxForm({
    form: 'signin', // nom du form
    fields: Object.keys(fields)
})(SigninPage);

export default withStyles(styles)(connect(null, actions)(signinForm));