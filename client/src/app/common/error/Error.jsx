import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as actions from './duck/ErrorAction';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import {withRouter} from 'react-router';
class Error extends Component {
    componentWillUpdate(nextProps) {
        if (this.props.location != nextProps.location) {
            this.props.resetError();
        }
    }

    render() {
        const {error} = this.props;
        console.log(error);
        return (
            error &&
                <Alert severity="danger">
                    {error}
                </Alert>
        );
    }
}

const mapStateToProps = state => {
    return {
        error: state.error.message
    }
}
export default withRouter(connect(mapStateToProps, actions)(Error));