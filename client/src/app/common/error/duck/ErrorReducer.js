import {PARSE_ERROR, RESET_ERROR} from './ErrorTypes';

const initialState = {
    message: ''
}

/* reducer */
export default function ErrorReducer(state = initialState, action) {
    switch(action.type) {
        /* nouvel état */
        case PARSE_ERROR: 
            return {
                message: action.payload
            }
        case RESET_ERROR: 
            return {
                message: ''
            }
        default:
            return state;
    }
}