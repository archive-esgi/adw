import React, {Component} from 'react';
import {connect} from 'react-redux';
import {AppBar, Toolbar, Typography, Button, IconButton, Container, Grid} from '@material-ui/core/';
import {Link} from 'react-router-dom';
import {withStyles} from '@material-ui/core/styles';
import adwLogo from '../../../assets/img/logo/adw-logo-main.png';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1,
    },
    link: {
        margin: theme.spacing(1, 1.5),
        textDecoration: 'none'
    },
});

class Header extends Component {
    renderAuthLink = () => {
        const {classes} = this.props;

        if (this.props.isLoggedIn) {
            return (
                [
                    <Link to="/dashboard" className={classes.link}>
                        <Button color="primary" variant="outlined">
                            Mon Dashboard
                        </Button>
                    </Link>,
                    <Link to="/signout" className={classes.link}>
                        <Button color="primary" variant="outlined">
                            Déconnexion
                        </Button>
                    </Link>
                ]
            )
        } else {
            return (
                [
                    <Link to="/signin" className={classes.link}>
                        <Button color="primary" variant="outlined">
                            Connexion
                        </Button>
                    </Link>
                    ,
                    <Link to="/signup" className={classes.link}>
                        <Button color="primary" variant="outlined">
                            Inscription
                        </Button>
                    </Link>
                ]
            )
        }
    }
    
    render() {
        const {classes} = this.props;

        return (
            <React.Fragment>
                <div className={classes.root}>
                    <AppBar
                    title={<img src={adwLogo}/>}
                    position="relative"
                    color="default"
                    elevation={0}
                    className={classes.appBar}
                    >
                        <Toolbar className={classes.toolbar}>
                            <IconButton edge="start">
                                <img src={adwLogo} width="30" height="30" />
                            </IconButton>
                            <Typography variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
                                <Link className={classes.link}>
                                </Link>
                            </Typography>
                            {this.renderAuthLink()}
                        </Toolbar>
                    </AppBar>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return { 
        isLoggedIn: state.authentification.isLoggedIn
    };
};

export default withStyles(styles)(connect(mapStateToProps)(Header));