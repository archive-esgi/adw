import React from 'react';
import {Typography, Grid, Box} from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import Copyright from '../copyright/Copyright';

const useStyles = makeStyles(theme => ({
    footer: {
        padding: theme.spacing(3, 2),
        marginTop: '20px',
        backgroundColor:
        theme.palette.type === 'dark' ? theme.palette.grey[800] : theme.palette.grey[200],
    },
    link: {
        textDecoration: 'none',
        listStyle: 'none',
        margin: 0,
        padding: 0
    }
}));

const footers = [
    {
        title: 'A propos',
        description: ['L\'équipe', 'Notre Histoire'],
    },
    {
        title: 'Contact',
        description: ['Adresse', 'contact@lesagentsduweb.com', '08 800 800 800'],
    }
];

export default function StickyFooter() {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Container component="footer">
                <Grid container spacing={4} justify="space-evenly">
                {footers.map(footer => (
                    <Grid item xs={6} sm={3} key={footer.title}>
                        <Typography variant="h6" color="textPrimary" gutterBottom>
                            {footer.title}
                        </Typography>
                        <ul className={classes.link}>
                            {footer.description.map(item => (
                            <li key={item} className={classes.link}>
                                <Link href="#" variant="subtitle1" color="textSecondary">
                                    {item}
                                </Link>
                            </li>
                            ))}
                        </ul>
                    </Grid>
                ))}
                </Grid>
                <Box mt={2} mb={3}>
                    <Copyright />
                </Box>
            </Container>
        </React.Fragment>
    );
}