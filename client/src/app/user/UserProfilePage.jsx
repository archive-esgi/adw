import React, { Component } from 'react';
import Header from '../common/header/Header';
import UserProfile from './UserProfile';

class UserProfilePage extends Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <UserProfile />
            </React.Fragment>
        );
    }
}

export default UserProfilePage;