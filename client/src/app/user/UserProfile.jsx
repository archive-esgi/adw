import React, {Component} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import {Divider, Avatar, Box, Card, CardContent, CardActions, Button, Typography, Toolbar, Drawer, Hidden, IconButton, List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core/';
import Dashboard from '@material-ui/icons/Dashboard';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { makeStyles } from '@material-ui/core/styles';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MissionTable from '../mission/MissionTable';
import ValidateTable from '../validatePoint/ValidateTable';
import {Link} from 'react-router-dom';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0,
        },
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
        display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    link: {
        textDecoration: 'none'
    },
    large: {
        width: theme.spacing(12),
        height: theme.spacing(12),
    },
});

class DashboardDrawer extends Component {
    drawer = () => {
        const {classes} = this.props;

        return (
            <div>
                <div className={classes.toolbar} />
                <Divider />
                <List>
                    {['Dashboard', 'Mon profil', 'Les missions'].map((text, index) => (
                        <ListItem button key={text}>
                            <ListItemIcon>{index % 2 === 0 ? <Dashboard /> : <AccountCircle />}</ListItemIcon>
                            <ListItemText primary={text} />
                        </ListItem>
                    ))}
                </List>
            </div>
        );
    }
            
    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
            <CssBaseline />
            <nav className={classes.drawer} aria-label="mailbox folders">
                <Hidden smUp implementation="css">
                <Drawer
                    classes={{
                    paper: classes.drawerPaper,
                    }}
                >
                    {this.drawer()}
                </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                <Drawer
                    classes={{
                    paper: classes.drawerPaper,
                    }}
                    variant="permanent"
                    open
                >
                    {this.drawer()}
                </Drawer>
                </Hidden>
            </nav>
            <main className={classes.content}>
                <Box mb={-7}></Box>
                <Box mb={-7}></Box>

                <div className={classes.toolbar} />
                <Grid container spacing={3}>
                    <Grid item sm={12}>
                        <Card className={classes.root}>
                            <CardContent>
                                <Avatar src="/static/images/avatar/1.jpg" className={classes.large} />
                                <Box mb={2}>  </Box>
                                <Typography variant="h6" color="textSecondary" component="h4">
                                    CERTIFIE
                                </Typography>
                                <Typography variant="h6" color="textSecondary" component="h4">
                                    Jean DUJARDIN
                                </Typography>
                                <Typography variant="h6" color="textSecondary" component="h4">
                                    SIRET : 362 521 879 00034
                                </Typography>
                                <br />
                                <Typography variant="h6" color="textSecondary" component="h4">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec velit odio, faucibus a lectus vel, mollis porttitor nibh. Cras ullamcorper quis lectus id volutpat. In vitae convallis ligula. Aenean non pellentesque urna, eget ullamcorper massa. Morbi dignissim neque vitae lacinia facilisis. Aenean non fringilla ex. Suspendisse semper nisi et dolor pretium, sed mollis libero maximus. Integer tincidunt mattis euismod. Morbi sed diam sed sapien luctus volutpat. Nam bibendum nisi est, a vehicula velit dignissim faucibus. Curabitur est lorem, viverra ac justo sit amet, tempor sodales nisl.
                                </Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item sm={12}>
                        <Card className={classes.root}>
                            <CardContent>
                                <Typography variant="h6" color="textSecondary" component="h4">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec velit odio, faucibus a lectus vel, mollis porttitor nibh. Cras ullamcorper quis lectus id volutpat. In vitae convallis ligula. Aenean non pellentesque urna, eget ullamcorper massa. Morbi dignissim neque vitae lacinia facilisis. Aenean non fringilla ex. Suspendisse semper nisi et dolor pretium, sed mollis libero maximus. Integer tincidunt mattis euismod. Morbi sed diam sed sapien luctus volutpat. Nam bibendum nisi est, a vehicula velit dignissim faucibus. Curabitur est lorem, viverra ac justo sit amet, tempor sodales nisl.
                                </Typography>
                                <br />
                                <Typography variant="h6" color="textSecondary" component="h4">
                                    Expérience : 5 ans
                                </Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </main>
            </div>
        );
    }
}

export default withStyles(styles)(DashboardDrawer);