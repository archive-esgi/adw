import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Table, Button, TableBody, TableCell, TableContainer, TableHead, TableRow} from '@material-ui/core/';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    table: {
        width: 100
    },
});

function createData(id, lastName, firstName, job) {
    return {id, lastName, firstName, job};
}

const rows = [
    createData(1, 'Dujardin', 'Jean', 'Développeur web')
];

export default function UserTable() {
    const classes = useStyles();

    return (
        <TableContainer component={Paper}>
            <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>#</TableCell>
                        <TableCell>Nom</TableCell>
                        <TableCell>Prénom</TableCell>
                        <TableCell>Poste</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {rows.map(row => (
                    <TableRow key={row.name}>
                        <TableCell>{row.id}</TableCell>
                        <TableCell>{row.lastName}</TableCell>
                        <TableCell>{row.firstName}</TableCell>
                        <TableCell>{row.job}</TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}