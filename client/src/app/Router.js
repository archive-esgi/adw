import React from 'react';
import {Switch, Route} from 'react-router-dom';
import HomeComponent from './home/HomeComponent';
import SigninPage from './authentification/SigninPage';
import SignupPage from './authentification/SignupPage';
import SignoutPage from './authentification/SignoutPage';
import MissionsPage from './mission/MissionsPage';
import SingleMissionPage from './mission/SingleMissionPage';
import DashboardPage from './dashboard/DashboardPage';
import WikiPage from './wiki/WikiPage';
import UserProfilePage from './user/UserProfilePage';

// helpers
import RequireAuth from './helpers/RequireAuth';

function Router() {
    return (
        <div>
            <Switch>
                <Route exact path="/" component={HomeComponent} />
                <Route path="/home" component={HomeComponent} />
                <Route path="/signin" component={SigninPage} />
                <Route path="/signup" component={SignupPage} />
                <Route path="/signout" component={SignoutPage} />
                <Route exact path="/missions" component={RequireAuth(MissionsPage)} />
                <Route exact path="/missions/10" component={RequireAuth(SingleMissionPage)} />
                <Route path="/dashboard" component={RequireAuth(DashboardPage)} />
                <Route exact path="/missions/10/wiki" component={RequireAuth(WikiPage)} />
                <Route path="/profile" component={RequireAuth(UserProfilePage)} />
            </Switch>
        </div>
    );
}

export default Router;
