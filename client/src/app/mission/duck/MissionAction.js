import axios from 'axios';
import {BASE_URL} from '../../../Constants';
import {
    GET_MISSIONS,
    GET_MISSION,
    POST_MISSIONS,
    DELETE_MISSION
} from './MissionTypes';

export function getMissions() {
    return function(dispatch) {
        axios.get(`${BASE_URL}/missions`, {
            headers: {
                authorization: localStorage.getItem('token')
            }
        })
        .then(res => {
                dispatch({
                type: GET_MISSIONS,
                payload: res.data
            })
        }).catch((err) => {
            console.log(err);
        })
    }
}

export function getMissionById(id) {
    return function(dispatch) {
        axios.get(`${BASE_URL}/missions/${id}`)
        .then(res => {
                dispatch({
                type: GET_MISSION,
                payload: res.data
            })
        }).catch((err) => {
            console.log(err);
        })
    }
}

export function deleteMission(id) {
    return function(dispatch) {
        axios.delete(`${BASE_URL}/missions/${id}`)
        .then(res => {
            dispatch({
                type: DELETE_MISSION,
                payload: id
            });
        })
    }
}

export function createMission(mission) {
    return function(dispatch) {
        axios.post(`${BASE_URL}/missions`, {
            title: mission.title,
            content: mission.content,
            slug: mission.slug
        }).then((response) => {
            dispatch({type: POST_MISSIONS, payload: response.data})
        });
    }
}