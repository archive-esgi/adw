import React, { Component } from 'react';
import Header from '../common/header/Header';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import Chip from '@material-ui/core/Chip';
import TagFacesIcon from '@material-ui/icons/TagFaces';
import Paper from '@material-ui/core/Paper';

class MissionsPage extends Component {
    state = {
        chipData: [
            { key: 0, label: 'Angular' },
            { key: 1, label: 'Graphisme' },
            { key: 2, label: 'Gestion de projet' },
            { key: 3, label: 'Vidéo' },
            { key: 4, label: 'Vue.js' },
        ]
    }
    
    
    render() {
        const classes = this.props;
        const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        const useStyles = makeStyles(theme => ({
            icon: {
              marginRight: theme.spacing(2),
            },
            heroContent: {
              backgroundColor: theme.palette.background.paper,
              padding: theme.spacing(8, 0, 6),
            },
            cardGrid: {
              paddingTop: theme.spacing(8),
              paddingBottom: theme.spacing(8),
            },
            card: {
              height: '100%',
              display: 'flex',
              flexDirection: 'column',
            },
            cardMedia: {
              paddingTop: '56.25%', // 16:9
            },
            cardContent: {
              flexGrow: 1,
            },
            footer: {
              backgroundColor: theme.palette.background.paper,
              padding: theme.spacing(6),
            },
          }));
        return (
            <React.Fragment>
                <Header />
                <main className={classes.content}>
                        <div className={classes.heroContent}>
                            <Container>
                                <Typography component="h2" variant="h2" color="textPrimary">
                                Missions disponibles
                                </Typography>
                            </Container>
                        </div>

                        <Container className={classes.cardGrid}>
                        <Grid container spacing={4} >
                            {cards.map(card => (
                            <Grid item key={card} xs={12} sm={6} md={4}>
                                <Card className={classes.card}>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image="https://source.unsplash.com/random"
                                    title="Image title"
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="h5" component="h2">
                                    <a href='#'>Création d'un projet WEB</a>
                                    </Typography>
                                    <Typography>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In venenatis eget mauris ut viverra. Nullam volutpat, est at viverra ultricies, ipsum ipsum facilisis elit, quis tincidunt tellus felis sit amet dolor.
                                    </Typography>
                                </CardContent>
                                <CardActions>
                                <Box className={classes.root}>
                                    {this.state.chipData.map(data => {
                                        let icon;

                                        if (data.label === 'React') {
                                        icon = <TagFacesIcon />;
                                        }

                                        return (
                                        <Chip
                                            key={data.key}
                                            icon={icon}
                                            label={data.label}
                                            className={classes.chip}
                                        />
                                        );
                                    })}
                                </Box>
                                </CardActions>
                                </Card>
                            </Grid>
                            ))}
                        </Grid>
                        </Container>
                    </main>
            </React.Fragment>
        );
    }
}

export default MissionsPage;