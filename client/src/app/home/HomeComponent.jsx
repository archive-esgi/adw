import React from 'react';
import Header from '../common/header/Header';
import { makeStyles } from '@material-ui/core/styles';
import {Typography, Button, Container, Grid} from '@material-ui/core/';
import homeBg from '../../assets/img/home-bg.png';
import Footer from '../common/footer/Footer';
import HomeSection1 from './HomeSection1';
import HomeSection2 from './HomeSection2';

const useStyles = makeStyles(theme => ({
    menuButton: {
        marginRight: theme.spacing(2),
    },
    mainBg: {
        backgroundImage: `url(${homeBg})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        display: 'flex',
        justifyContent: 'center', 
        alignItems: 'center', 
        flexDirection: 'column',
        height: '100vh'
    }
}));

function HomeComponent() {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Header />
            <div className={classes.mainBg} />
            <HomeSection1 />
            <HomeSection2 />
            <Footer />
        </React.Fragment>
    );
}

export default HomeComponent;