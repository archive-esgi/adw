import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import sectionTwo from '../../assets/img/sectionTwo.png';

const useStyles = makeStyles(theme => ({
    cardHeader: {
        backgroundColor:
        theme.palette.type === 'dark' ? theme.palette.grey[700] : theme.palette.grey[200],
    },
    mainBg: {
        backgroundImage: `url(${sectionTwo})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        display: 'flex',
        justifyContent: 'center', 
        alignItems: 'center', 
        flexDirection: 'column',
        height: '100vh'
    }
}));

const tiers = [
    {
        title: 'Evénementiel',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent gravida velit ac finibus laoreet. Nulla quis mi nisl. Phasellus in aliquet velit. Morbi eget maximus enim. Phasellus ac vestibulum justo, vitae aliquet tellus.',
        mdGrid: 6
    },
    {
        title: 'Expertise Web',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent gravida velit ac finibus laoreet. Nulla quis mi nisl. Phasellus in aliquet velit. Morbi eget maximus enim. Phasellus ac vestibulum justo, vitae aliquet tellus.',
        mdGrid: 6
    },
    {
        title: 'Communication visuelle',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus gravida odio leo, id hendrerit ex ornare vitae.  Vestibulum at efficitur nunc. In fermentum purus eget felis viverra, eu ultricies tortor hendrerit.',
        mdGrid: 6
    },
    {
        title: 'Accompagnement',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus gravida odio leo, id hendrerit ex ornare vitae.  Vestibulum at efficitur nunc. In fermentum purus eget felis viverra, eu ultricies tortor hendrerit.',
        mdGrid: 6
    },
    {
        title: 'Conception mobile',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus gravida odio leo, id hendrerit ex ornare vitae. Nullam elit dolor, blandit quis urna id, tincidunt vehicula odio. Vestibulum at efficitur nunc. In fermentum purus eget felis viverra, eu ultricies tortor hendrerit. ',
        mdGrid: 6
    },
    {
        title: 'Stratégie digitale',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus gravida odio leo, id hendrerit ex ornare vitae. Nullam elit dolor, blandit quis urna id, tincidunt vehicula odio. Vestibulum at efficitur nunc. In fermentum purus eget felis viverra, eu ultricies tortor hendrerit. ',
        mdGrid: 6
    },
];

export default function Pricing() {
  const classes = useStyles();

  return (
    <React.Fragment>
        <div className={classes.mainBg}>
            <Container maxWidth="md" component="main">
                <Grid container spacing={5} alignItems="flex-end">
                {tiers.map(tier => (
                    <Grid item key={tier.title} xs={12} md={tier.mdGrid}>
                    <Card>
                        <CardHeader
                        title={tier.title}
                        titleTypographyProps={{ align: 'center' }}
                        subheaderTypographyProps={{ align: 'center' }}
                        className={classes.cardHeader}
                        />
                        <CardContent>
                            <Typography variant="subtitle1" align="center">
                                {tier.description}
                            </Typography>
                        </CardContent>
                    </Card>
                    </Grid>
                ))}
                </Grid>
            </Container>
        </div>
    </React.Fragment>
  );
}