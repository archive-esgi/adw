import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Table, Button, TableBody, TableCell, TableContainer, TableHead, TableRow} from '@material-ui/core/';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    table: {
        width: 100
    },
});

function createData(id, title, content, mission, state, help) {
    return {id, title, content, mission, state, help};
}

const rows = [
    createData(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Pellentesque ultrices interdum velit vitae vehicula. Fusce at iaculis justo.', 'Mission 10', 'En cours', ''),
    createData(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 'Pellentesque ultrices interdum velit vitae vehicula. Fusce at iaculis justo.', 'Mission 10', 'En cours', '')
];

export default function ValidateTable() {
    const classes = useStyles();

    return (
        <TableContainer component={Paper}>
            <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>#</TableCell>
                        <TableCell>Titre</TableCell>
                        <TableCell>Contenu</TableCell>
                        <TableCell>Mission</TableCell>
                        <TableCell>Etat</TableCell>
                        <TableCell>Aide</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {rows.map(row => (
                    <TableRow key={row.name}>
                        <TableCell>{row.id}</TableCell>
                        <TableCell>{row.title}</TableCell>
                        <TableCell>{row.content}</TableCell>
                        <TableCell>{row.mission}</TableCell>
                        <TableCell>{row.state}</TableCell>
                        <TableCell>{row.help}</TableCell>
                        <TableCell>
                            <Button href="/missions/10/wiki" variant="contained" color="primary">
                                Voir l'aide
                            </Button>
                        </TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}