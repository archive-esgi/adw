import React from 'react';
import ReactDOM from 'react-dom';
import Router from './app/Router';
import {BrowserRouter} from 'react-router-dom'; 
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import './assets/style/main.css';
import reducers from './rootReducers';
import {setAuthentification} from './app/authentification/duck/AuthAction';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#99bde0',
        },
    },
  });

const createStoreWithMiddleware = applyMiddleware(
    thunk,
)(createStore);

const store = createStoreWithMiddleware(
    reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ 
    && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const token = localStorage.getItem('token');

if (token) {
    store.dispatch(setAuthentification(true));
}

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Provider store={store}>
            <BrowserRouter>
                <Router />
            </BrowserRouter>
        </Provider>
    </ThemeProvider>
    ,document.getElementById('root')
);