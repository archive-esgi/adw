# Les Agents du Web

Plateforme de partage d'expérience et mise en relation entre des freelance/ agence et des offres  de missions pour le compte de l'agence 148.

## Nécessaire
* Docker
* Docker-compose
* Node

## Installation

Il faut démarrer les conteneurs docker dans le dossier api :

```bash
docker-compose up -d
```
Et ensuite démarrer le site :

```bash
npm install
npm run start
```

Puis lancer le front 

## Utilisation

Une fois le site démarré une page a dû s'ouvrir sinon elle doit être sur l'adresse indiqué dans l'invite de commande.