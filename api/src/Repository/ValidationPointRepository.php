<?php

namespace App\Repository;

use App\Entity\ValidationPoint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ValidationPoint|null find($id, $lockMode = null, $lockVersion = null)
 * @method ValidationPoint|null findOneBy(array $criteria, array $orderBy = null)
 * @method ValidationPoint[]    findAll()
 * @method ValidationPoint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ValidationPointRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ValidationPoint::class);
    }

    // /**
    //  * @return ValidationPoint[] Returns an array of ValidationPoint objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ValidationPoint
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
